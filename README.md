<h1>
   <img src="https://emojis.slackmojis.com/emojis/images/1562883039/5948/bongo_blob.gif?1562883039" width="30"/>
   Hey! I'm Angel.
</h1>

<p>Welcome to my page! </br> I'm Angel, a Software Engineer <img src="https://cdn-icons-png.flaticon.com/512/197/197397.png" width="18"/> <b>Mexico</b>.



Check out my latest project <code><a href="https://marketplace.visualstudio.com/items?itemName=Iamanaws.iamanaws-extension-pack"><img valign="middle" src="https://iamanaws.gallerycdn.vsassets.io/extensions/iamanaws/iamanaws-extension-pack/0.3.0/1630860373622/Microsoft.VisualStudio.Services.Icons.Default" width="15px" height="15px" /> My extension pack </a></code>

## Things I code with

<p>
   <img src="https://github.com/get-icon/geticon/raw/master/icons/bash.svg" width="24px" height="24px" />
   <img src="https://raw.githubusercontent.com/get-icon/geticon/master/icons/markdown.svg" width="24px" height="24px" />
   <img src="https://github.com/get-icon/geticon/raw/master/icons/git-icon.svg" width="24px" height="24px" />
   <img src="https://github.com/get-icon/geticon/raw/master/icons/github-icon.svg" width="24px" height="24px" />
   <img src="https://github.com/get-icon/geticon/raw/master/icons/javascript.svg" width="24px" height="24px" />
   <img src="https://raw.githubusercontent.com/get-icon/geticon/master/icons/html-5.svg" width="24px" height="24px" />
   <img src="https://raw.githubusercontent.com/get-icon/geticon/master/icons/css-3.svg" width="24px" height="24px" />
   <img src="https://github.com/get-icon/geticon/raw/master/icons/nodejs-icon.svg" width="24px" height="24px" />
   <img src="https://github.com/get-icon/geticon/raw/master/icons/npm.svg" width="24px" height="24px" />
   <img src="https://raw.githubusercontent.com/get-icon/geticon/master/icons/python.svg" width="24px" height="24px" />
   <img src="https://github.com/get-icon/geticon/raw/master/icons/prettier.svg" width="24px" height="24px" />
   <img src="https://github.com/get-icon/geticon/raw/master/icons/microsoft-windows.svg" width="24px" height="24px" />
   <img src="https://raw.githubusercontent.com/get-icon/geticon/master/icons/c-sharp.svg" width="24px" height="24px" />
   <img src="https://raw.githubusercontent.com/get-icon/geticon/master/icons/unity.svg" width="24px" height="24px" />
   <img src="https://github.com/get-icon/geticon/raw/master/icons/linux-tux.svg" width="24px" height="24px" />
   <img src="https://github.com/get-icon/geticon/raw/master/icons/ubuntu.svg" width="24px" height="24px" />
   <img src="https://raw.githubusercontent.com/get-icon/geticon/master/icons/kali-logo.svg" width="24px" height="24px" />
   <img src="https://raw.githubusercontent.com/get-icon/geticon/master/icons/fedora.svg" width="24px" height="24px" />
   <img src="https://raw.githubusercontent.com/get-icon/geticon/master/icons/perl.svg" width="24px" height="24px" />
   <img src="https://raw.githubusercontent.com/get-icon/geticon/master/icons/visual-studio-code.svg" width="24px" height="24px" />
   <img src="https://raw.githubusercontent.com/get-icon/geticon/master/icons/visual-studio.svg" width="24px" height="24px" color="white"/>
   <img src="https://raw.githubusercontent.com/get-icon/geticon/master/icons/intellij-idea.svg" width="24px" height="24px" />
   <img src="https://raw.githubusercontent.com/get-icon/geticon/master/icons/pycharm.svg" width="24px" height="24px" />
   <img src="https://raw.githubusercontent.com/get-icon/geticon/master/icons/mysql.svg" width="24px" height="24px" />
</p>
